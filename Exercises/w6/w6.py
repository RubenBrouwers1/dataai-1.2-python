import pandas as pd
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

socialMediaHours = pd.read_csv('socialMediaVsPoints.csv',delimiter=',')
#axis 0 = drow rows with na values, axis 1 = drop columns with na values
socialMediaHours.dropna(axis=0,inplace=True)
#socialMediaHours.replace('unknown', 0)
#socialMediaHours = socialMediaHours.drop(socialMediaHours[socialMediaHours['hours'] == 'unknown'], inplace=True)
socialMediaHours = socialMediaHours.drop(socialMediaHours.loc[socialMediaHours['hours'] == 'unknown'].index)
socialMediaHours['hours'] = socialMediaHours['hours'].astype(float)
socialMediaHours['points'] = socialMediaHours['points'].astype(float)
print(socialMediaHours.info())

#Put these seperately into a histogram
cutpoints = range(0, 5, 1)
classes = pd.cut(socialMediaHours['hours'], bins=cutpoints, right=False)

hoursSocialMedia = {'bins' : [],
				 	'values' : []}

hoursSocialMedia = pd.DataFrame(hoursSocialMedia,columns=['bins','values'])

hoursSocialMedia['bins'] = cutpoints
hoursSocialMedia['values'] = classes.value_counts().sort_index()
hoursSocialMedia.columns = ['bins','values']


def histogramChartHours():
	#from 0 to 10
	#change frequency to needed row
	#also works: frequency=[0,1,3,2,3,5,6,3,2,0]
	"""
	example, also works with arrays	
	dfq8 = {'score': ['1','2','3','4','5','6','7','8','9','10'],
		'frequency': [0,1,3,2,3,5,6,3,2,0]}
	dfq8 = pd.DataFrame(dfq8, columns=['score','frequency'])"""

	#score = range(1,socialMediaHours['hours'].astype(int).max(),1)
	plt.figure()
	ax = plt.axes()
	ax.set_xticks(hoursSocialMedia['bins'])
	plt.bar(hoursSocialMedia['bins'], hoursSocialMedia['values'])
	plt.show()


cutpoints = range(0, 20, 1)
classes = pd.cut(socialMediaHours['points'], bins=cutpoints, right=False)

pointsSocialMedia = {'bins' : [],
				 	'values' : []}

pointsSocialMedia = pd.DataFrame(pointsSocialMedia,columns=['bins','values'])

pointsSocialMedia['bins'] = cutpoints
pointsSocialMedia['values'] = classes.value_counts().sort_index()
pointsSocialMedia.columns = ['bins','values']

def histogramChartPoints():
	#from 0 to 10
	#change frequency to needed row
	#also works: frequency=[0,1,3,2,3,5,6,3,2,0]
	"""
	example, also works with arrays	
	dfq8 = {'score': ['1','2','3','4','5','6','7','8','9','10'],
		'frequency': [0,1,3,2,3,5,6,3,2,0]}
	dfq8 = pd.DataFrame(dfq8, columns=['score','frequency'])"""

	#score = range(1,socialMediaHours['hours'].astype(int).max(),1)
	plt.figure()
	ax = plt.axes()
	ax.set_xticks(pointsSocialMedia['bins'])
	plt.bar(pointsSocialMedia['bins'], pointsSocialMedia['values'])
	plt.show()

#c. What is the average number of hours a student spends on social media?
print(socialMediaHours['hours'].mean())
#d. What is the standard deviation of the number of hours? What does this mean?
print(socialMediaHours['hours'].std())
#e. What is the average score of the students?
print(socialMediaHours['points'].mean())
#f. What is the standard deviation of the score?
print(socialMediaHours['points'].std())
#g. Make a scatterplot of the 2 variables. What correlation do you expect?

def scatterPlotChartHours():
	plt.figure()
	plt.scatter(socialMediaHours['hours'], socialMediaHours['points'])
	plt.show()

def calculateRegressionLine():
	#just change these two values
	x = socialMediaHours['hours']
	y = socialMediaHours['points']
	model = np.polyfit(x, y, 1) #calculates b and a
	print(model)
	predict = np.poly1d(model)
	xx = np.arange(x.min(), x.max(), (x.max()-x.min())/100)
	yy = predict(xx)
	plt.figure()
	plt.scatter(x, y)
	plt.plot(xx, yy, color='red')
	plt.show()

#Question 2: battery defects
batteriesProduced = pd.read_csv('batteries.csv',delimiter=',')
print(batteriesProduced)

def scatterPlotChartBatteries():
	plt.figure()
	plt.scatter(batteriesProduced['#Produced'], batteriesProduced['#Defects'])
	plt.show()

#batteriesProduced.columns = ['Produced','Defect']
def get_outliers(data):
	quartiles = data.quantile(q=[0.25, 0.5, 0.75])
	Q1 = quartiles.iloc[0]
	Q3 = quartiles.iloc[2]
	IQR = Q3 - Q1
	return data[(data<(Q1 - IQR * 1.5)) | (data > (Q3 + IQR * 1.5))]



def get_extreme_outliers(data):
	quartiles = data.quantile(q=[0.25, 0.5, 0.75])
	Q1 = quartiles.iloc[0]
	Q3 = quartiles.iloc[2]
	IQR = Q3 - Q1
	return data[(data<(Q1 - IQR * 3)) | (data > (Q3 + IQR * 3))]

print(get_outliers(batteriesProduced).dropna())
print(get_extreme_outliers(batteriesProduced).dropna())

#c. What correlation do you find with Kendall's method?
print(batteriesProduced.corr(method='kendall'))

#d. We are actually not interested in the absolute number of defective batteries, but we are interested in the percentage. So divide the number of defective batteries by the number of batteries produced. What is the average percentage of defective batteries?

#batteriesProduced['Percentage'] = 100*(batteriesProduced['#Defects'].sum() / batteriesProduced['#Produced'].sum())
print(8000*(batteriesProduced['#Defects'].sum() / batteriesProduced['#Produced'].sum()))