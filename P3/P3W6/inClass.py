import np as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats
import math

# Coherence
linkedin = pd.read_csv('csv/linkedIn.csv', delimiter=",")
studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter=';', decimal=',')
# g = pd.read_csv('depincrc02.csv', delimiter=";")

# Correlation: Scatterplot
plt.figure()
plt.scatter(linkedin['# Connections'], linkedin['Salary'])
plt.show()

# Correlation coefficient
linkedin_scaled = pd.DataFrame(stats.zscore(linkedin))
cor_pear = (linkedin_scaled[0]*linkedin_scaled[1]).mean()
print(cor_pear)

# x.cor(y)
# dataFrame.cor(method='pearson')

cor_pear = linkedin['# Connections'].corr(linkedin['Salary'])
print(linkedin.corr(method='pearson'))

# print(g.corr(method='pearson'))

# Correlation Meaning
# ---------------------
# 0 -          No linear relationship
# 0 to 0,2 -   Hardly any linear relationship
# 0,2 to 0,4 - Weak linear relationship
# 0,4 to 0,6 - Decent linear relationship
# 0,6 to 0,8 - Strong linear relationship
# 0,8 to 1 -   Very strong linear relationship

# Rank correlation

connectionCutPoints = [0, 30, 150, 250, 300, 400, 600]
connectionCategories = ['little',
                        'moderate',
                        'average',
                        'more',
                        'much', 'very much']
salaryCutPoints = [0, 2000, 3000, 5000, 6000]
salaryCategories = ['small', 'medium', 'large', 'extreme']
connections = pd.cut(linkedin['# Connections'], bins=connectionCutPoints)
connections.cat.categories = connectionCategories
salary = pd.cut(linkedin['Salary'], bins=salaryCutPoints)
salary.cat.categories = salaryCategories

salaryRank = salary.rank()
connectionsRank = connections.rank()

# Kendall
# Based on counting the number of cordante and
# discordante data couples
# g.corr(method= 'kendall')

def kendall(x, y):
    x = x.rank()
    y = y.rank()
    n = len(x)
    number = 0
    for i in range(1, n - 1):
        for j in range(i + 1, n):
            number = number + np.sign(x[i] - x[j]) * np.sign(y[i] - y[j])
    return 2 * number / n / (n - 1)

print('Kendall')
print(kendall(connections, salary))

# Spearman
# Based on replacing the values with rank
# numbers and calculating the correlation
# g.corr(method= 'spearman')

# Linear regression
# Can I predict one variable numerically based on
# value of the other?

# y = a + b * x --> x: independent variable, y: dependent variable & a: height of the line, b: the slope of the line
a = 50
b = 30
x = linkedin['# Connections']
y = linkedin.Salary
predicted = a + b * x
error = y - predicted
se = math.sqrt((error ** 2).mean())
print(se)

# Reliability
x = linkedin['# Connections']
y = linkedin.Salary
model = np.polyfit(x, y, 1) #calculates b and a
print(model)
predict = np.poly1d(model)
xx = np.arange(x.min(), x.max(), (x.max() - x.min())/100)
yy = predict(xx)
plt.figure()
plt.scatter(x, y)
plt.plot(xx, yy, color='red')
plt.show()

# Declared variance
# linear regression
R_squared = 1 - (x.corr(y))** 2

# in general
y_pred = predict(x) # define first the predict function!
se = np.sqrt(((y_pred - y)**2).mean())
sy = y.std()
R_squared = 1 --(se**2)/(sy**2)

print(R_squared)

# Non linear regression
# model parameters
# TODO: Extend this function so that it includes logaritmic regression
def general_regression(x, y, degree=1, exp=False, log=True):
    data = pd.DataFrame({'x': x, 'y': y})
    data.reset_index(drop=True, inplace=True)
    func = lambda x:x
    inv_func = lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    elif (log):
        x = np.log(x)
    sy = data.y.std()
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x: func(line(x))
    data['y_pred'] = pd.Series(predict(x))
    se = np.sqrt(((data.y_pred-data.y) ** 2).mean())
    R2 = 1-(se ** 2)/(sy ** 2)
    result = [se, R2, predict]
    index = ['se', R2, predict]
    for i in range(1, len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) #To obtain the characters
    result = pd.Series(result)
    result.index = index
    return result

result = general_regression(x, y, 2)
print(result)
result = general_regression(x, y, 3)
print(result)
result = general_regression(x, y, 1, exp=True)
print(result)

# TODO: Extend this function so that in includes exponectial and logaritmic regression
def plot_regressionline(reg_result, min, max, linecol='red', errorcol='#FFFF0080', exp=False, log=False):
    se = reg_result.se
    x = np.arange(min, max, (max-min)/100)
    predict = reg_result.predict
    if (log):
        x = np.log(x)
    # if (exp):

    y = predict(x)
    plt.fill_between(x, y-se, y+se, color=errorcol)
    plt.plot(x, y, color=linecol)


plt.figure()
plt.scatter(x, y)
plot_regressionline(result, x.min(), x.max(), log=True)
plt.xlabel('number connections')
plt.ylabel('salary(EUR)')
plt.show()

# TODO: Make the function: best_fit_Rsquare (x,y)