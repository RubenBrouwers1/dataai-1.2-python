from durable.lang import *

with ruleset('expense'):
    @when_all((m.subject == 'approve') | (m.subject == 'ok'))
    def approved(c):
        print ('Approved subject: {0}'.format(c.m.subject))
        
post('expense', { 'subject': 'approve'})