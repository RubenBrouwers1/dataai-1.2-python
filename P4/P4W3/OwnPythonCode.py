# Decision Trees
import pandas as pd
import numpy as np

simpsons = pd.read_csv('The Simpsons.csv' , delimiter=',')

def entropy(column: pd.Series, base=None):
    # Determine the fractions for all column values v
    fracties = column.value_counts(normalize=True, sort=False)
    base = 2 if base is None else base
    return -(fracties * np.log(fracties) / np.log(base)).sum()

def information_gain(df: pd.DataFrame, s: str, target: str):
    # calculate entropy of parent table
    entropy_parent = entropy(df[target])
    child_entropies = []
    child_weights = []
    # compute entropies of child tables
    for (label, p) in df[s].value_counts().items():
        child_df = df[df[s] == label]
        child_entropies.append(entropy(child_df[target]))
        child_weights.append(int(p))
    # calculate the difference between parent entropy and weighted child entropies
    return entropy_parent - np.average(child_entropies, weights=child_weights)

def best_split(df:pd.DataFrame, target: str):
    # retrieve all non-target column labels (the features))
    features = df.drop(axis=1,labels=target).columns
    # calculate the information gains for these features
    gains = [information_gain(df,feature,target) for feature in features]
    # return column with highest information gain
    return features[np.argmax(gains)], max(gains)

# entropy(simpsons.gender)

for label in simpsons.drop(labels='gender', axis=1).columns:
    print('{}: {}'.format(label, information_gain(simpsons, label, 'gender')))

best_split(simpsons, 'gender')