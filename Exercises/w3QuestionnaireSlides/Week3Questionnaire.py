import numpy as np
import pandas as pd

studenq = pd.read_csv('Questionnaire 20-21.csv', delimiter=";", decimal=",")
studenq.columns
studenq.dtypes

studenq['Blood Type'] = studenq['Blood Type'].astype(pd.CategoricalDtype(categories=studenq['Blood Type'].unique()))
print(studenq.dtypes)

df_selection = studenq[['Writing Hand', 'Blood Type', 'Smoker']]
df_selection.columns = ['C1', 'C2', 'C3']
df_selection = df_selection.dropna(axis='rows')
print(studenq['Favorite Fruit'].head())

#split --> 1=Strawberry,...
def split_choices(row):
	return str.split(row, ',')

choices_matrix = studenq['Favorite Fruit'].apply(split_choices)
print(choices_matrix)