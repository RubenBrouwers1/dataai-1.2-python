import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import math
import os

linkedin = pd.read_csv('csv/linkedIn.csv', delimiter=',')

# TODO < < < ~ CORRELATION  &  REGRESSION ~ > > >
# there are 2 types of correlation --> positive and negative
# todo < Causality >
plt.figure()
plt.scatter(linkedin['# Connections'], linkedin['Salary'])
plt.xlabel('Total Connections')
plt.ylabel('Salary')
plt.show()
# does not demonstrates the actual relation between 2 arguments

# TODO << Correlation Coefficient of Pearson >>
linkedin_scaled = pd.DataFrame(stats.zscore(linkedin))

# manual calculations for correlations
cor_pear = (linkedin_scaled[0] * linkedin_scaled[1]).mean()
print(cor_pear)

# calculating with built-in function
cor_pear2 = (linkedin['# Connections'].corr(linkedin['Salary']))
print(cor_pear2)

# Correlation Meaning
# ---------------------
# 0 -          No linear relationship
# 0 to 0,2 -   Hardly any linear relationship
# 0,2 to 0,4 - Weak linear relationship
# 0,4 to 0,6 - Decent linear relationship
# 0,6 to 0,8 - Strong linear relationship
# 0,8 to 1 -   Very strong linear relationship

# todo < Ranking Correlations >
# all values must be an interval value
connectionCutPoints = [0, 30, 150, 250, 300, 400, 600]
connectionCategories = ['little',
                        'moderate',
                        'average',
                        'more',
                        'much', 'very much']
salaryCutPoints = [0, 2000, 3000, 5000, 6000]
salaryCategories = ['small', 'medium', 'large', 'extreme']
connections = pd.cut(linkedin['# Connections'], bins=connectionCutPoints)
connections.cat.categories = connectionCategories
salary = pd.cut(linkedin['Salary'], bins=salaryCutPoints)
salary.cat.categories = salaryCategories

salaryRank = salary.rank()
connectionsRank = connections.rank()

# todo < Spearman >
connectionsRank.corr(salaryRank)


# todo < Kendall >


def kendall(x, y):
    x = x.rank()
    y = y.rank()
    n = len(x)
    number = 0
    for i in range(1, n - 1):
        for j in range(i + 1, n):
            number = number + np.sign(x[i] - x[j]) * np.sign(y[i] - y[j])
    return 2 * number / n / (n - 1)


print(kendall(connections, salary))
# shortcut >> >> >>
connectionsRank.corr(salaryRank, method='kendall')

# TODO #2 <<< Linear Regression >>>
# TODO #2.1 Determining the lines >>> >>>
# y = a + b * x --> x: independent variable, y: dependent variable & a: height of the line, b: the slope of the line
a = 50
b = 30
x = linkedin['# Connections']
y = linkedin.Salary
predicted = a + b * x
error = y - predicted
se = math.sqrt((error ** 2).mean())

# TODO #2.5 << Regression in Python >>
# finding a & b values via using polyfit() method
x = linkedin['# Connections']
y = linkedin.Salary
model = np.polyfit(x, y, 1)

print(model)

a = model[1]
b = model[0]
print(f'a:{a} \nb:{b}')

predict = np.poly1d(model)
predict(200)

se2 = math.sqrt(((predict(x) - y) ** 2).mean())
print(se2)

# visualizing the data we have
plt.figure()
plt.scatter(x, y)
xx = np.arange(x.min(), x.max(), (x.max() - x.min()) / 100)
yy = predict(xx)
plt.fill_between(xx, yy - se2, yy + se2, color='#FFFF8080')
plt.plot(xx, yy, color='red')
plt.xlabel('Total Connections')
plt.ylabel('Salary (USD $)')
plt.show()

# TODO #2.7 << Expressed Variance >>
r_squared = x.corr(y) ** 2  # result --> 73% relation to salary, the 27% is due unknown reasons


# TODO #3 << Non-Linear Regression >>


def general_regression(ar1, ar2, degree=1, exp=False):
    func = lambda x:x  # def fun(x): return[x]
    inv_func = lambda x:x
    if (exp):
        func = np.exp
        inv_func = np.log
    model = np.polyfit(ar1, inv_func(ar2), degree)
    line = np.poly1d(model)
    predict = lambda ar1: func(line(x))
    yyy = pd.Series(predict(ar1))
    se = math.sqrt(((yyy - ar2) ** 2).mean())
    R2 = (ar1.corr(inv_func(ar2))) ** 2
    result = [se, R2, predict]
    index = ['se', 'R²', 'predict']
    for i in range(1, len(model) + 1):
        result = np.append(result, model[-i])
    index += chr(i + 96)  # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result


# degree: the degree of the polynomial you are looking for (1=linear, 2=quadratic, 3=cubic)
result = general_regression(x, y, 3)
print(result)
