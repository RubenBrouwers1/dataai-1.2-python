import scipy.spatial.distance as dist
import pandas as pd
from sklearn.neighbors import DistanceMetric
import numpy as np
from scipy import stats
import matplotlib
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier

studenq = pd.read_csv("Questionnaire 20-21.csv", delimiter=';', decimal=',')

# Create a new data frame stud_numbers containing the following columns from
# the dataframe studenq: Shoe Size, Length, Siblings, Mobile Devices, Travel
# Distance, Biden
stud_numbers = studenq[['Shoe Size', 'Length', 'Siblings', 'Mobile Devices', 'Travel Distance', 'Biden']].copy()

# Drop rows with nan-value(s)
stud_numbers = stud_numbers.dropna(axis='rows')

# What is the maximum Manhattan distance between the different students?
# from sklearn.neighbors import DistanceMetric
# dist_obj = DistanceMetric.get_metric('manhattan')
# dist_obj.pairwise(exampletrans).max()

# Perform a hierarchical clustering on the data in stud_numbers
# and review the dendrogram. Which students are the "misfits"?
from sklearn.cluster import AgglomerativeClustering
stud_numbers = stud_numbers.reset_index(drop=True)
hcmodel = AgglomerativeClustering(n_clusters=4)
hcmodel.fit(stud_numbers)
from scipy.cluster.hierarchy import linkage, dendrogram
colors = list(matplotlib.colors.cnames.keys())
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(5, 5))
distances = linkage(stud_numbers, method='single')
ax.set_title("Dendrogram")
ax.set_xlabel('point')
ax.set_ylabel('Euclidian distance') 
ax.grid(linestyle='--', axis='y') 
dgram = dendrogram(distances, labels=list(range(1, stud_numbers.shape[0]+1)), link_color_func=lambda x: colors[x], leaf_font_size=15., ax=ax) 
plt.show()
