import numpy as np
import pandas as pd

from datetime import datetime as dt

delays18 = pd.read_csv('https://raw.githubusercontent.com/nickdcox/learn-airline-delays/main/delays_2018.csv')
delays19 = pd.read_csv('https://raw.githubusercontent.com/nickdcox/learn-airline-delays/main/delays_2019.csv')
#filter out every column except airport and airport_name
airportCoordinates = pd.read_csv('https://raw.githubusercontent.com/nickdcox/learn-airline-delays/main/airport_coordinates.csv')

delays = pd.concat([delays18, delays19], axis=0)

delays["date"] = pd.to_datetime(delays["date"], format='%Y-%m')

#delays = delays.dropna(subset = ["arr_flights", "carrier", "airport"])
#delays = delays.dropna(subset = ["carrier"])
#delays = delays.dropna(subset = ["airport"])
#delays = delays.drop(delays[(delays["date"].dt.year < 2018)].index | delays[(delays["date"].dt.year > 2019)].index)
#delays = delays.drop(delays[(delays["date"].dt.year > 2019)].index)

delays = delays[(delays.date >= '2018-01')&
				(delays.date <= '2019-12')&
				(delays.arr_flights.notnull())&
				(delays.carrier.notnull())&
				(delays.carrier_name.notnull())&
				(delays.airport.notnull())&
				(delays.airport_name.notnull())]

print(delays)
print("Amount of rows: " + str(len(delays)) + "\n")
airportsInTennessee = delays[delays.airport_name.str.contains('TN:')]
print("Airports in Tennessee: ")
print(airportsInTennessee["airport_name"])

airports = delays[['airport', 'airport_name']].drop_duplicates().reset_index(drop=True)
airports = airports.merge(airportCoordinates, on='airport')

print(airports[airports.airport.str.contains('^B.*$', regex=True, na=False)].mean())