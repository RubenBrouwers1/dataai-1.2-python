import pandas as pd
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

q1Scores = {'score' : [17,18,19,20,21,22,23,24],
			'f' : [5,6,17,23,32,10,10,7]}
q1Scores = pd.DataFrame(q1Scores,columns=['score','f'])

#Give range of test (max score - min score)
def giveRange(data):
	return max(data) - min(data)

#Give interquartile distance (of score)
def giveInterquartileDistance(data):
	#also works: stats.iqr(q1Scores.score.dropna())/2
	return (data.quantile(0.75) - data.quantile(0.25))/2

#Draw a box plot
def showBoxPlot(data):
	plt.figure()
	#change to what you need
	dataToPlot = ['score']
	data.dropna().boxplot(dataToPlot)
	plt.show()

showBoxPlot(q1Scores)

#Give standard deviation (of score)
def giveStandardDeviation(data):
	return data.dropna().std()

def get_outliers(data):
    quartiles = data.quantile(q=[0.25, 0.5, 0.75])
    Q1 = quartiles.iloc[0]
    Q3 = quartiles.iloc[2]
    IQR = Q3 - Q1
    return data[(data<(Q1 - IQR * 1.5)) | (data > (Q3 + IQR * 1.5))]

def get_extreme_outliers(data):
    quartiles = data.quantile(q=[0.25, 0.5, 0.75])
    Q1 = quartiles.iloc[0]
    Q3 = quartiles.iloc[2]
    IQR = Q3 - Q1
    return data[(data<(Q1 - IQR * 3)) | (data > (Q3 + IQR * 3))]

q2Scores = { 'score' : [22,24,26,30,32,35],
			 'f' : [1,2,4,6,3,4]}
q2Scores = pd.DataFrame(q2Scores, columns=['score','f'])

def central(data):
	mode = data.mode()
    #for dataframe, for np array:
	#mode = stats.mode(data)
	median = data.median()
    #for dataframe, for np array:
	#median = np.median(data)
	mean = data.mean()
	geometric = np.exp(np.mean(np.log(data)))
	harmonic = 1/np.mean(1/data)
	return print("Mode: " + str(mode) + "\nMedian :" + str(median) + "\nMean: " + str(mean) + "\nGeometric: " + str(geometric) + "\nHarmonic: " + str(harmonic))

def calculateWeightedAverage(score, weight):
	"""Example np arrays
	weight = [4,2,3,3]
	score = [20,21,25,26]"""

	return np.average(score, weights=weight)

def calculateVariance(data):
	return data.dropna().var()

def calculateZScore(data):
	return stats.zscore(data)

"""
The mean of a variable (X) is 20, the median 18, the variance 6 and the interquartile distance
4. To all scores 10 is added.
a. What is the new mean? --> mean of variable X + 10 = 30
b. What is the new median? --> median of variable X + 10 = 28
c. What is the new variance?
d. What is the new interquartile distance?
"""

"""
From a frequency distribution, all scores are divided by 4. What do we know about the
standard deviation of these new scores?
4x as small as the original score
"""

"""
The average age of this class is 20 years old with a standard deviation of 2. Within 20 years
there will be a reunion. Everyone is still alive and present.
How big is the mean and the standard deviation over 20 years?
40 means and 2 standard deviation (everybody just ages by 20)
"""
"""
The distribution of scores has a mean of 8 with a standard deviation of 2. We want to transform
the scores in such a way that the mean equals 10 and the standard deviation equals 4. What
should we do with all the scores?
multiply by 2 and then reduce by 6
"""

logs = pd.read_csv('UofS_access_log.txt', delimiter=' ')

logs.drop(logs.columns[1], axis=1, inplace=True)
logs.drop(logs.columns[1], axis=1, inplace=True)
logs.drop(logs.columns[2], axis=1, inplace=True)
logs.columns = ['domain','timestamp','request','response_code','response_length']
logs = logs.replace('-',0)

logs['response_length'] = pd.to_numeric(logs['response_length'], errors='coerce')

#logs = logs.sort_values(by='response_length', ascending=False)

"""cutpoints = range(0, logs['response_length'].max() + 10000, 10000)
classes = pd.cut(logs['response_length'], bins=cutpoints, right=False, labels=False)"""

logsLessThan13000Bytes = logs.loc[logs['response_length'] < 13000]
cutpoints = range(0, logsLessThan13000Bytes['response_length'].max() + 1000, 1000)
classes = pd.cut(logs['response_length'], bins=cutpoints, right=False, labels=False)

logsDataFrame = {'bins' : [],
				 'values' : []}

logsDataFrame = pd.DataFrame(logsDataFrame,columns=['bins','values'])

"""logsDataFrame['bins'] = cutpoints
logsDataFrame['values'] = pd.cut(logs['response_length'], bins=cutpoints, right=False)"""
logsDataFrame['bins'] = cutpoints
logsDataFrame['values'] = classes.value_counts().sort_index()
logsDataFrame.columns = ['bins','values']

def histogramQ8():
	#from 0 to 10
	#change frequency to needed row
	#also works: frequency=[0,1,3,2,3,5,6,3,2,0]
	"""
	example, also works with arrays	"""
	dfq8 = {'score': ['1','2','3','4','5','6','7','8','9','10'],
		'frequency': [0,1,3,2,3,5,6,3,2,0]}
	dfq8 = pd.DataFrame(dfq8, columns=['score','frequency'])

	score = range(1,11,1)
	plt.figure()
	ax = plt.axes()
	ax.set_xticks(logsDataFrame['bins'])
	plt.bar(logsDataFrame['bins'], logsDataFrame['values'])
	plt.show()

"""logs.response_length"""
"""print(len(cutpoints))
print(classes.value_counts().sort_index().count())"""

"""plt.figure()
ax = plt.axes()
ax.set_xticks(logs.response_length)
plt.bar(logs.response_length, classes.value_counts().sort_index())
plt.show()"""