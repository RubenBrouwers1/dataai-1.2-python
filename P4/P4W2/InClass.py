# Forecasting

import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt

revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]

plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

# Forecasting based on the past
# Naive forecasting
def naiveForecasting(past):
    if (len(past)<1):
        return math.nan
    return past[len(past)-1]

print("Naive forecasting: ", naiveForecasting(revenues))

past = revenues
predicted = [ ]
for i in range(0,4):
    next = naiveForecasting(past)
    predicted = predicted + [next]
    past = past + [next]

plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(21, 25), predicted, 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

# Average of all previous values
def averageForecasting(past):
    if (len(past)<1):
        return math.nan
    return pd.Series(past).mean()

print("Average of all previous values: ", averageForecasting(revenues))

past = revenues
predicted = [ ]
for i in range(0,4):
    next = averageForecasting(past)
    predicted = predicted + [next]
    past = past + [next]

plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(21, 25), predicted, 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

# Moving average
def movingAverageForecasting(past, period):
    n = len(past)
    if (n < period):
        return math.nan
    return pd.Series(past[(n-period):n]).mean()

# def movingAverageForecasting(period):
#     def result(past):
#         n = lens(past)
#         if (n < period):
#             return m.nan
#         return pd.Series(past[(n-period):n]).mean()
#     return result

print("Moving average: ", movingAverageForecasting(revenues, 4))

past = revenues
predicted = [ ]
for i in range(0,4):
    next = movingAverageForecasting(past, 4)
    predicted = predicted + [next]
    past = past + [next]

plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(21, 25), predicted, 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

# Linear combination
predictedTest = [287.9464, 393.2149, 543.8526, 318.7714]

def calculateWeights(period, past):
    n = len(past)
    if (n<2*period):
        return math.nan
    v = past[(n-2*period):(n-period)]
    for i in range(2,period+1):
        v = v + past[(n-2*period+i-1):(n-period+i-1)]
    M = np.array(v).reshape(period, period)
    v = past[(n-period):n]
    return np.linalg.solve(M, v)

a = calculateWeights(4, revenues)
print(a)

period = 4
n = len(revenues)
prediction = (revenues[(n-period):n]*a).sum()
print(prediction)

def linearCombinationForecasting(period):
    def result(past):
        n = len(past)
        if (n<2*period):
            return math.nan
        a = calculateWeights(period, past)
        return (past[(n-period):n]*a).sum()
    return result

forecast = linearCombinationForecasting(4)
print(forecast(revenues))

past = revenues
predicted = [ ]
for i in range(0,4):
    forecast = linearCombinationForecasting(4)
    next = forecast(past)
    predicted = predicted + [next]
    past = past + [next]

for i in predicted:
    print(i)


plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(21, 25), predicted, 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

# Reliabilty of the model
def calculatePreviousForecasting(past, predictor):
    predicted = []
    n = len(past)
    for i in range(0,n):
        predicted = predicted + [predictor(past[0:i])]
    return predicted

predicted = calculatePreviousForecasting(revenues, naiveForecasting)
print(predicted)

plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(0,20),  calculatePreviousForecasting(revenues, naiveForecasting), 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

# Errors
predicted = calculatePreviousForecasting(revenues, naiveForecasting)
errors = pd.Series(predicted) - revenues
MAE = errors.abs().mean() 
print('MAE= ', MAE) # the MAE (Mean Absolute Error)
RMSE = math.sqrt((errors**2).mean())
print('RMSE= ', RMSE) # the RMSE (Root Mean Squared Error)
MAPE = (errors/revenues).abs().mean()
print('MAPE= ', MAPE) # the MAPE (Mean Absolute Percentage Error)

def mae(past, predictor):
    predicted = calculatePreviousForecasting(past, predictor)
    errors = pd.Series(predicted) - past
    return errors.abs().mean()

def rmse(past, predictor):
    predicted = calculatePreviousForecasting(past, predictor)
    errors = pd.Series(predicted) - past
    return math.sqrt((errors**2).mean())

def mape(past, predictor):
    predicted = calculatePreviousForecasting(past, predictor)
    errors = pd.Series(predicted) - past
    return (errors/past).abs().mean()

def movingAverageForecastingError(period):
    def result(past):
        n = len(past)
        if (n < period):
            return math.nan
        return pd.Series(past[(n-period):n]).mean()
    return result

print(mae(revenues, movingAverageForecastingError(4)))
print(rmse(revenues, movingAverageForecastingError(4)))
print(mape(revenues, movingAverageForecastingError(4)))

# Creating a model for the data

# Trend forecasting
def general_regression(x, y, degree=1, exp=False):
    func=lambda x:x # def fun(x): return[x]
    inv_func=lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x:func(line(x))
    yyy = pd.Series(predict(x))
    se = math.sqrt(((yyy-y)**2).mean())
    R2 = (x.corr(inv_func(y)))**2
    result = [se, R2, predict]
    index = ['se', 'R2', 'predict']
    for i in range(1,len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

def trendForecastingModel(past):
    n = len(past)
    x = pd.Series(range(0,n))
    y = pd.Series(past)
    reg = general_regression(x, y)
    return reg.predict

myTrend = trendForecastingModel(revenues)
print(myTrend(20))

trendForcasting = [0]*20

for i in range(0, 20):
    trendForcasting[i] = myTrend(i)

# for value in trendForcasting:
#     print(value)

plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(0,20),  trendForcasting, 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()

predicted = pd.Series(myTrend(range(0,20)))
errors = predicted - revenues
print(errors)

MAE = errors.abs().mean()
print('MAE= ', MAE)
RMSE = math.sqrt((errors**2).mean())
print('RMSE= ', RMSE)
MAPE = (errors/revenues).abs().mean()
print('MAPE= ', MAPE)

# TODO: Add the Seasonal Forcasting part to this file;

# TODO: additive & multiplicative function 

# Seasonal decomposition
import statsmodels.tsa.seasonal as smts

result=smts.seasonal_decompose(
revenues, model='additive', period=4)

