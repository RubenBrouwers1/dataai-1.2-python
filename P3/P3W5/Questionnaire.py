import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats

studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter=';', decimal=',')


'''def central(data):
    mode = data.mode()
    median = data.median()
    average = data.mean()
    geometric = np.exp(np.mean(np.log(data)))
    harmonic = 1 / np.mean(1 / data)
    return print("Mode: %8.2f, Median: %6.2f, Average: %5.2f, Geometric Average: %8.2f, Harmonic Average: %8.2f" % (
    mode, median, average, geometric, harmonic))'''

def central(data):
    mode = data.mode()
    #for dataframe, for np array:
    #mode = stats.mode(data)
    median = data.median()
    #for dataframe, for np array:
    #median = np.median(data)
    mean = data.mean()
    geometric = np.exp(np.mean(np.log(data)))
    harmonic = 1/np.mean(1/data)
    return print("Mode: " + str(mode) + "\nMedian :" + str(median) + "\nMean: " + str(mean) + "\nGeometric: " + str(geometric) + "\nHarmonic: " + str(harmonic))


def get_outliers(data):
    quartiles = data.quantile(q=[0.25, 0.5, 0.75])
    Q1 = quartiles.iloc[0]
    Q3 = quartiles.iloc[2]
    IQR = Q3 - Q1
    return data[(data < (Q1 - IQR * 1.5)) | (data > (Q3 + IQR * 1.5))]


def get_extreme_outliers(data):
    quartiles = data.quantile(q=[0.25, 0.5, 0.75])
    Q1 = quartiles.iloc[0]
    Q3 = quartiles.iloc[2]
    IQR = Q3 - Q1
    return data[(data < (Q1 - IQR * 3)) | (data > (Q3 + IQR * 3))]

# a function that print the different measurements of statistical dispersion)
def dispersion (series):
    s = series.dropna()
    import numpy as np
    if type(s[0]) == np.int32 or type(s[0]) == np.float32 or type(s[0]) == np.float64 or type(s[0]) == np.int64:
        d_range = s.dropna().max() - s.dropna().min()
        d_IQR = s.dropna().quantile(0.75) - s.dropna().quantile(0.25)
        d_mad = s.dropna().mad()
        d_var = s.dropna().var()
        d_std = s.dropna().std()
        print(" Range IQR MAD Var std")
        print ('%5.2f' %d_range + ' ' + '%5.2f' %d_IQR + ' ' +'%5.2f' %d_mad + ' ' +'%5.2f' %d_var + ' ' +'%5.2f' %d_std)
    else:
        print ('Range = ' + s.min() + ' ' + s.max())


# 1.a Which shoe size is most common among the
# students?
print('1a')
print(studenq['Shoe Size'].mode())

# 1.b What shape has the distribution of the shoe size?
print('1b, look graph')
# TODO: This gives multiple modes... how do i fix this
central(studenq['Shoe Size'])
plt.figure()
studenq['Shoe Size'].plot.hist()
plt.title('Shoe size students')
plt.xlabel('Shoe size')
plt.show()

# Are there outliers in the data regarding the shoe
# size ?
print('2a')
print((get_outliers(studenq['Shoe Size'].dropna())).count())
print('2b')
print((get_outliers(studenq['Length'].dropna())).count())

# A student has a shoe size 44 and a length of 175.
# Has this student seen relatively (in the group)
# larger feet or a larger length ?
print('3a')
print((44 - studenq['Shoe Size'].mean())/studenq['Shoe Size'].dropna().std())
print((175 - studenq['Length'].mean())/studenq['Shoe Size'].dropna().std())

# 4.a What can you deduce from comparing the different
# measurements of statistical dispersion regarding the number
# of mobile devices? (Tip: Write a function that print the different
print('4a')
dispersion(studenq['Mobile Devices'])
plt.figure()
data_to_plot = ['Mobile Devices']
studenq.boxplot(data_to_plot)
plt.show()

# What shape has the distribution function of the number of
# mobile devices?
print('4a')
central(studenq['Mobile Devices'])
plt.figure()
studenq['Mobile Devices'].plot.hist()
plt.title('Usse Mobile Devices by Students')
plt.xlabel('Number of mobile devices')
plt.show()