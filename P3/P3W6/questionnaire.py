import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter=';', decimal=',')

def split_choices(row):
    return str.split(row, ',')

def split_preferences(row):
    return np.array([str.split(el, '=') for el in row])

def sort_preferences(row):
    try:
        ranknumbers = row[:, 0].astype(int)  # first colomn contains the ranknumbers
        return row[ranknumbers.argsort()][:, 1]  # second colomn contains the types of fruit
    except Exception:  # eg. when no numbers used
        return np.repeat(np.nan, 10)

# 1.A Make a Python function that transforms the data as presented in
# the table on the previous page into this table.
fruit_preferences = pd.DataFrame(zip(*studenq['Favorite Fruit'].apply(split_choices).apply(split_preferences).apply(sort_preferences)), index=range(1,11)).transpose()

def transform_preference(df):
    order = df.loc[0].sort_values()
    index = pd.Series(order, dtype="category")
    copy_df = df.copy()
    for column in copy_df:
        copy_df[column] = copy_df[column].astype(pd.CategoricalDtype(categories=order))
        copy_df[column] = copy_df[column].cat.codes
    new_df = pd.DataFrame(index = index, columns=np.arange(copy_df.shape[0]))
    for i in range(copy_df.shape[0]):
        for j in range(copy_df.shape[1]):
            new_df.iat[copy_df.iat[i,j],i]=j+1
    for column in new_df:
        new_df[column] = pd.to_numeric(new_df[column])
    return new_df

fruit_pref_trans = transform_preference(fruit_preferences)
fruit_pref_trans = fruit_pref_trans.dropna(axis='columns')
print(fruit_pref_trans)

# 1.b To what extent do Student002 and Student007
# agree in terms of preference for fruit?
fruit_pref_trans[2].corr(fruit_pref_trans[7], method='kendall')
print(fruit_pref_trans.corr(method='kendall'))

# 2. Is there a (linear) relationship between the number of
# siblings and the number of mobile devices a student uses?
# What correlation are you going to use here?
# What's your conclusion?

cor_pear = studenq['Siblings'].corr(studenq['Mobile Devices'])
print(cor_pear)

# 3. Is there a (linear) relationship between how important a
# student thinks his computer science studies are and how he sees
# the study load? What correlation are you going to use here?
studenq['Importace AI Study'] = studenq['Importace AI Study'].astype(pd.CategoricalDtype(categories=['Not at all','Little importance', 'Moderate importance', 'Great importance', 'Very great importance', 'Extreemly important'], ordered=True))
studenq['Study Taks'] = studenq['Study Taks'].astype(pd.CategoricalDtype(categories=['<= 10 hours', '11 15 hours', '16 20 hours', '21 25 hours', '26 30 hours', '36 40 hours','>= 41 hours'], ordered=True))
df = pd.DataFrame({'Importance':studenq['Importace AI Study'],'Load':studenq['Study Taks']})
df.dropna(axis='rows', inplace=True)
df.Importance = df.Importance.cat.codes
df.Load = df.Load.cat.codes
df.corr(method='kendall')
df.Importance.corr(df.Load, method='kendall')
print(df)

# Keith Pledger's textbook "Edexcel GCSE Mathematics" claims
# that (for boys) the length in cm is equal to 5.3 times the shoe
# size plus 133. What formula do you get for the computer science
# students? Note: English shoe size not equal to European shoe size

# A. Converting from
# European shoe size to
# English shoe size:
# (y uk = a + b x eu)
# Use the data from the given
# table and make a graph.

eu = [39,40,40.5,41,42,42.5,43,44,44.5,45,46,46.5,47,48]
uk = [5,6,6.5,7,8,8.5,9,10,10.5,11,12,12.5,13,14]
model = np.polyfit(eu, uk, 1)
predict = np.poly1d(model)
shoe_uk = predict(studenq['Shoe Size'])

x = pd.Series(eu)
y= pd.Series(uk)
xx = np.arange(x.min(), x.max(), (x.max() - x.min()) / 100)
yy = predict(xx)
plt.figure()
plt.scatter(x, y)
plt.plot(xx, yy, color='red')
plt.xlabel('Shoe size (EU)')
plt.ylabel('Shoe size (UK)')
plt.show()

# b. Perform a linear regression and make a
df_4b = pd.DataFrame({'Shoe_uk':shoe_uk, 'Length':studenq['Length']})
df_4b.dropna(axis='rows', inplace=True)
x = df_4b.Shoe_uk
y = df_4b.Length
model = np.polyfit(x, y, 1)
predict = np.poly1d(model)
xx = np.arange(x.min(), x.max(), (x.max() - x.min()) / 100)
yy = predict(xx)
plt.figure()
plt.scatter(x, y)
plt.plot(xx, yy, color='red')
plt.xlabel('Shoe size (UK)')
plt.ylabel('Length (cm)')
plt.show()
print('se %5.2f'% np.sqrt(((predict(x) - y) ** 2).mean()))