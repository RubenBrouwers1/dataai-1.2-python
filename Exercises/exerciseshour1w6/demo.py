import pandas as pd


#Question 1
delays18 = pd.read_csv('https://raw.githubusercontent.com/nickdcox/learn-airline-delays/main/delays_2018.csv')
delays18.head()
delays18.info()

delays19 = pd.read_csv('https://raw.githubusercontent.com/nickdcox/learn-airline-delays/main/delays_2019.csv')
delays19.head()
delays19.info()

delays = delays18.append(delays19, ignore_index=True)
delays.info()

len(delays)

delays.date = pd.to_datetime(delays.date,format='%Y-%m')
#.dt.tz_localize('America/New_York')
delays.info()