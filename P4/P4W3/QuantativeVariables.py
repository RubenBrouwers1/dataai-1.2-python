from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

simpsons = pd.read_csv("The Simpsons original.csv")
classifier = DecisionTreeClassifier(criterion='entropy')
classifier.fit(simpsons.drop(['gender', 'name'], axis=1), simpsons['gender'])
tree.plot_tree(classifier, feature_names=simpsons.drop(['gender', 'name'], axis=1).columns.values, class_names=['cluster {}'.format(i) for i in np.unique(simpsons['gender'])],filled=True)

plt.show()