import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats

arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])
#For question 3
dfq3 = {'x':['4','5','6','7','8','9','10'],
   		'frequency':[2,3,5,6,4,2,3]}
  
dfq3 = pd.DataFrame(dfq3, columns=['x','frequency'])
 

#Question 2: pie chart
def showPieChartq2():
	"""
	example arrays	
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])"""

	dfq2 = {'Fruit' : ['apple','banana','grapes','pear','melon','kiwi'],
			'Frequency' : [17,5,3,1,2,2]}

	dfq2 = pd.DataFrame(dfq2, columns=['Fruit','Frequency'])

	figPie, axPie = plt.subplots()
	axPie.pie(dfq2.Frequency, labels=dfq2.Fruit)
	axPie.axis('equal')
	plt.show()

#Question 2: bar chart
def showBarChartq2():
	"""
	example arrays	"""
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])

	yPos = np.arange(len(arrayFruitNames))
	plt.bar(yPos, arrayFrequencyNumbers, align='center', alpha=0.5)
	plt.xticks(yPos, arrayFruitNames)
	plt.ylabel('Frequency')
	plt.title('Most favourite fruit')
	plt.show()

#Question 3: what cumulative percentage is associated with score 6?
def cumulativePercentageDataFrame(dataFrame):
	#change frequency with needed row
	#adds another row called "Cum Percent", print this dataframe to show result
	#also = percentile score
	dataFrame['Cum Percent'] = 100*(dataFrame.frequency.cumsum() / dataFrame.frequency.sum())

#Question 3: What percentile score is associated with score 8?
def percentileScoreDataFrame(dataFrame):
	#replace frequency with needed row
	#adds another row called "Percentile Rank", print this dataframe to show result
	dataFrame['Percentile Rank'] = dataFrame.frequency.rank(pct = True)

#Question 4: (in combination with past functions), complete the frequency table with relative frequencies, cum percent and percentile scores
def relativeFrequenciesDataFrame(dataFrame):
	#works?
	series = pd.Series(arrayFrequencyNumbers).value_counts() / len(arrayFrequencyNumbers)
	dataFrame['Frequencies'] = series

#Cumulative frequency function
def cumulativeFrequenciesDataFrame(dataFrame):
	#change frequency with needed row
	#adds another row called "Cum Frequency", print this dataframe to show result
	dataFrame['Cum Frequency'] = dataFrame.frequency.cumsum()

#Absolute frequency function
def absoluteFrequenciesDataFrame(dataFrame):
	dataFrame['Absolute Frequency'] = dataFrame.frequency.value_counts(normalize=True),

#Question 8: The scores of these variables can theoretically vary from 1 to 10. Turn these frequencies into a histogram with 5 classes covering the entire theoretical range.
#Question 8: Which cumulative percentage is associated with score 6? --> cumulativePercentageDataFrame(dataframe)
dfq8 = {'score': ['1','2','3','4','5','6','7','8','9','10'],
		'frequency': [0,1,3,2,3,5,6,3,2,0]}
dfq8 = pd.DataFrame(dfq8, columns=['score','frequency'])
def histogramQ8():
	#from 0 to 10
	#change frequency to needed row
	#also works: frequency=[0,1,3,2,3,5,6,3,2,0]
	"""
	example, also works with arrays	"""
	dfq8 = {'score': ['1','2','3','4','5','6','7','8','9','10'],
		'frequency': [0,1,3,2,3,5,6,3,2,0]}
	dfq8 = pd.DataFrame(dfq8, columns=['score','frequency'])

	score = range(1,11,1)
	plt.figure()
	ax = plt.axes()
	ax.set_xticks(score)
	plt.bar(score, dfq8.frequency)
	plt.show()

fileSatisfaction = open ('satisfaction.txt','r')
dfq10valueArray = np.array([])

for line in fileSatisfaction:
	stripLine = line.rstrip()
	np.append(dfq10valueArray, stripLine)

dfq10 = {'Score': ['1','2','3','4','5','6','7','8','9','10']}
dfq10 = pd.DataFrame(dfq10, columns=['Score'])

showBarChartq2()

