import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats

#rb = reading, binary --> when b isn't present an error will occur.
#this file is massive, prepare for lag
#fileWebServer = open('UofS_access_log.txt','rb')

"""
This was all wrong from the beginning >:(
dfWebServer = pd.DataFrame(dfWebServer, columns=['domain','a','b','timestamp','request','c','response_code','response_length'])
with open('UofS_access_log.txt', encoding='Latin1') as f:
	reader = csv.reader(f, delimiter=' ')
	for row in reader:
		dfWebServer = row
print(dfWebServer)
"""


#remove column 1,1,2
dfWebServer.drop(dfWebServer.columns[1], axis=1, inplace=True)
dfWebServer.drop(dfWebServer.columns[1], axis=1, inplace=True)
dfWebServer.drop(dfWebServer.columns[2], axis=1, inplace=True)
dfWebServer.columns = ['domain','timestamp','request','response_code','response_length']
dfWebServer = dfWebServer.replace('-',0)

#Question 1: What response code is most common? --> print(dfWebServer['response_code'].value_counts()) = 200

"""Question 2: What percentage of the requests contain a response code larger than 200?"""
dfWebServer['percentage'] = 100*(dfWebServer['response_code']/dfWebServer['response_code'].sum())
print((dfWebServer['percentage'].loc[dfWebServer['response_code'] > 200]).sum())

"""Question 3: how many times does a response code 401 occur?
dfWebServer['percentage'] = 100*(dfWebServer['response_code']/dfWebServer['response_code'].sum())

---Both solutions work---
print((dfWebServer.loc[dfWebServer['response_code'] == 401]).count())
print(dfWebServer['response_code'].value_counts())"""

"""Question 4: Which web pages have a response code 401?
print(dfWebServer['request'].loc[dfWebServer['response_code'] == 401].unique())
"""

"""Question 5: Which clients (domains) tried to consult a web page without authorization (401)?
print(dfWebServer['domain'].loc[dfWebServer['response_code'] == 401].unique())"""

"""Question 6: Which client (domain) most often attempted to access an unauthorized web page (401)?
print(dfWebServer['domain'].loc[dfWebServer['response_code'] == 401].mode())"""

"""Question 7: Make a pie chart of the response codes that are not equal to 200
def showPieChart():

	example arrays
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])

	values = dfWebServer['response_code'].value_counts()
	#drop index 200
	values = values.drop(200)
	labels = np.array(dfWebServer['response_code'].loc[dfWebServer['response_code'] != 200].unique())

	figPie, axPie = plt.subplots()
	axPie.pie(values, labels=labels)
	axPie.axis('equal')
	plt.show()
	print(values)
	print(labels)

showPieChart()
"""

"""Question 8: Create a bar graph of the same response codes
def showBarChart():

	example arrays
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])


	values = dfWebServer['response_code'].value_counts()
	#drop index 200
	values = values.drop(200)
	labels = np.array(dfWebServer['response_code'].loc[dfWebServer['response_code'] != 200].unique())

	yPos = np.arange(len(labels))
	plt.bar(yPos, values, align='center', alpha=0.5)
	plt.xticks(yPos, labels)
	plt.show()

showBarChart()	"""

"""Question 10: How big is the longest response that was sent?
#to select multiple columns: dfWebServer[['response_length','request']] --> double square brackets

print(dfWebServer[['response_length','request']].loc[dfWebServer['response_length'] == dfWebServer['response_length'].max()])"""

"""Question 11: How many responses have a response_length smaller than 1000 bytes?
print(dfWebServer.loc[pd.to_numeric(dfWebServer['response_length']) < 1000].count())"""

"""Question 12: What percent of the responses are between 1000 and 2000 bytes?
print(100*(dfWebServer[pd.to_numeric(dfWebServer['response_length']).between(1000, 2000)].count() / dfWebServer.count()))"""

"""Question 13: What percent of the responses are smaller than 6000 bytes?
print(100*(dfWebServer[pd.to_numeric(dfWebServer['response_length']) < 6000].count() / dfWebServer.count()))"""

"""Question 14: Is a 5000-6000 byte response large or small? --> Large, more than 81% of all response lengths are less than 5000 bytesprint(100*(dfWebServer[pd.to_numeric(dfWebServer['response_length']) < 5000].count() / dfWebServer.count()))"""

"""Question 15: Draw a bar chart of the absolute frequencies
def showBarChart():
	example arrays
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])


	values = dfWebServer['response_code'].value_counts()
	labels = np.array(dfWebServer['response_code'].unique())

	yPos = np.arange(len(labels))
	plt.bar(yPos, values, align='center', alpha=0.5)
	plt.xticks(yPos, labels)
	plt.show()

showBarChart()"""

"""Question 16: Draw the frequency polygon of the absolute frequencies
def showFrequencyPolygon():
	cutpoints = [0,1000,5000,6000,10000,15000,20000,30000]
	classes = pd.cut(pd.to_numeric(dfWebServer.response_length), bins=cutpoints)
	x = classes.value_counts().sort_index().plot()
	plt.show()

showFrequencyPolygon()"""

"""Question 17: draw a bar chart of the cumulative frequencies
doesn't work

def showBarChart():
	example arrays
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])


	values = pd.to_numeric(dfWebServer['response_length']).cumsum()
	labels = np.array(dfWebServer['response_length'].unique())

	yPos = np.arange(len(labels))
	plt.bar(yPos, values, align='center', alpha=0.5)
	plt.xticks(yPos, labels)
	plt.show()

def showBarChartq2():
	example arrays
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])

	values = pd.to_numeric(dfWebServer['response_length']).cumsum()
	labels = np.array(dfWebServer['response_length'].unique())

	yPos = np.arange(len(labels))
	plt.bar(yPos, values, align='center', alpha=0.5)
	plt.xticks(yPos, label)
	plt.ylabel('Frequency')
	plt.title('Most favourite fruit')
	plt.show()

def cumulativeFrequenciesDataFrame(dataFrame):
	#change frequency with needed row
	#adds another row called "Cum Frequency", print this dataframe to show result
	dataFrame['Cum Frequency'] = dataFrame.response_length.cumsum()

cumulativeFrequenciesDataFrame(dfWebServer)"""

"""Question 18: Make a bar chart of the absolute frequencies of the dates
def showBarChart():
	example arrays
	arrayFruitNames = np.array(["Apple", "Banana", "Grapes", "Pear", "Melon", "Kiwi"])
	arrayFrequencyNumbers = np.array([17, 5, 3, 1, 2, 2])

	values = dfWebServer['response_code'].value_counts(normalize=True)
	labels = np.array(dfWebServer['response_code'].unique())

	yPos = np.arange(len(labels))
	plt.bar(yPos, values, align='center', alpha=0.5)
	plt.xticks(yPos, labels)
	plt.show()"""
