import os
import pandas as pd
import matplotlib.pyplot as plt
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'

import graphviz
import six
import sys
sys.modules['sklearn.externals.six'] = six
from id3 import Id3Estimator, export_graphviz, export_text

simpsons = pd.read_csv("The Simpsons original.csv")
model = Id3Estimator()
# X = features, y = target
X = simpsons.drop(['name', 'gender'], axis=1).values.tolist()
Y = simpsons['gender'].values.tolist()
model.fit(X,Y)
print(export_text(model.tree_, feature_names=simpsons.drop(['name', 'gender'], axis=1).columns))

# Export decision tree to disk
export_graphviz(model.tree_,'tree.dot', feature_names=simpsons.drop(['name', 'gender'], axis=1).columns)

dot = graphviz.Source.from_file('tree.dot')
dot.render('tree', view=True)

# If the code below does not work, use the 2 line above

# # visualize decision tree
# graphviz.Source.from_file('tree.dot')
# ! dot -Tpdf tree.dot -o tree.pdf
# img = plt.imread('tree.pdf')
# plt.figure(figsize = (20, 20))
# plt.imshow(img)
# plt.show()