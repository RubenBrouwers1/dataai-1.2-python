import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

clusterExample = pd.read_csv('csv/clustering example.csv')
kmeansmodel = KMeans(n_clusters=4)
kmeansmodel.fit(clusterExample)

from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion='entropy')
classifier.fit(clusterExample, kmeansmodel.labels_)

tree.plot_tree(classifier, feature_names=['x', 'y'], class_names=['cluster {}'.format(i) for i in np.unique(kmeansmodel.labels_)],filled=True)
plt.show()

from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion='entropy')
classifier.fit(clusterExample, kmeansmodel.labels_)
tree.plot_tree(classifier, feature_names=['x', 'y'],
class_names=['cluster {}'.format(i) for i in np.unique(kmeansmodel.labels_)],filled=True)
plt.show()