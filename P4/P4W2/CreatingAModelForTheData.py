# Trend forecasting 

import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt

revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]

def general_regression(x, y, degree=1, exp=False):
    func=lambda x:x # def fun(x): return[x]
    inv_func=lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x:func(line(x))
    yyy = pd.Series(predict(x))
    se = math.sqrt(((yyy-y)**2).mean())
    R2 = (x.corr(inv_func(y)))**2
    result = [se, R2, predict]
    index = ['se', 'R2', 'predict']
    for i in range(1,len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

def trendForecastingModel(past):
    n = len(past)
    x = pd.Series(range(0,n))
    y = pd.Series(past)
    reg = general_regression(x, y)
    return reg.predict

myTrend = trendForecastingModel(revenues)
print(myTrend(21))

predicted = pd.Series(myTrend(range(0,20)))
errors = predicted - revenues
print(errors)

