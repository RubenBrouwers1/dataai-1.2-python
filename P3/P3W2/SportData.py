import matplotlib.pyplot as plt
import pandas as pd

sportData = pd.read_csv('csv/sportData.csv', delimiter=';')
sportData['Hours per week'].fillna(value=0,inplace=True)
sportData.plot.scatter(x='Times per week', y='Hours per week', title= 'Weekly sport effort')
plt.show()
