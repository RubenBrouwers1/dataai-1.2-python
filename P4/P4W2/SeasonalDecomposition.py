# Seasonal decomposition
import statsmodels.tsa.seasonal as smts
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt

revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]

print(smts.seasonal_decompose(revenues, model='additive', period=4))

# Determining the noise
result=smts.seasonal_decompose(revenues, model='additive', period=4)
print(result.trend)
print(result.seasonal)
print(result.resid)

plt.figure()
result.plot()
plt.show()

# If you want more control over the plot, you can also build it yourself with your own calculated
# trend, seasonal trend and noise:

plt.figure(figsize=(8,8))
plt.subplot(411)
plt.plot(range(0,20), revenues, 'o-')
plt.ylabel('observed')
plt.title('Additive decomposition')
plt.subplot(412)
# TODO: WTF is trend
plt.plot(range(0,20), trend, 'o-')
reg = general_regression(pd.Series(range(2,18)),
pd.Series(trend[2:18]), 3)
plt.ylabel('trend')
plt.subplot(413)
plt.plot(range(0,20), season*5, 'o-')
plt.ylabel('season')
plt.subplot(414)
plt.scatter(range(0,20), noise)
plt.ylabel('residue')
plt.xlabel('quarter')
plt.show()