import csv
import pandas as pd

df = pd.read_csv('namefile.csv', delimiter=';')

# If not all lines(observations) have the same number of columns,
# Python will generate an error:
#   pandas.errors.ParserError: Error tokenizing data. C error: Expected 2 fields in line 4, saw 3
# If so read in the csv-file line by line and correct the surplus of colums(concatenation)

fields = []
rows = []
with open('namefile.csv', newline='') as csvfile:
    data = csv.reader(csvfile, delimiter=';')
    fields = next(data)
    dfB = pd.DataFrame(columns=[fields[0], fields[1]])
    for rows in data:
        if len(rows) > 2:
            newRow = {fields[0]: rows[0], fields[1]: rows[1]+','+rows[2]}
            dfB = dfB.append(newRow, ignore_index=True)
        else:
            newRow = {fields[0]: rows[0], fields[1]: rows[1]}
            dfB = dfB.append(newRow, ignore_index=True)

# Check the data types(misread numbers will have object as type instead of int64, float64, ..
df.dtypes
# OR
df.info()

# Read the file again with the proper decimal point character (. or ,)
df = pd.read_csv('namefile.csv', delimiter=';', decimal='.')

# Identify which read-in values are NA-values and make necessary adjustments
missing_values = ['n/a', 'na', 'nan', 'N/A', 'NA', 'NaN',
                  'NAN', '--', 'Missing', 'missing', 'Unknown', 'unknown']
df = pd.read_csv('namefile.csv', delimiter=';',
                 decimal='.', na_values=missing_values)

# Check if there are NA-values
df.info()
# OR
df.isna.sum()

# Do they need to be changed in any specific values (e.g. na= > 0)?
df.fillna(0, inplace=True)

# Do they need to be removed?
df = df.dropna(axis='rows')  # Remove the rows where there is at least 1 NA
# OR
# Remove the columns where there is at least 1 NA
df.dropna(axis='columns', inplace=True)

# Check the data types for nominal and ordinal variables. Make the necessary adjustments
df['nominal variable'] = df['nominal variable'].astype(
    pd.CategoricalDtype(categories=df['nominal variable'].unique()))
df['ordinal variable'] = df['ordinal variable']. astype(pd.CategoricalDtype(
    categories=['lowest value', ..., 'highest value'], ordered=True))
