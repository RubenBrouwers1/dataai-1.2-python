#Exercise 1
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
import math
import numpy as np
import statsmodels.tsa.seasonal as smts
from statsmodels.tsa.seasonal import seasonal_decompose

revenues = [20,100,175,13,37,136,245,26,75,155,326,48,92,202,384,82,176,282,445,181]

data = pd.read_csv("popularityApp.csv")
n = len(data['date'])
downloads = data['downloads']

#a. Make a plot of the data so that you can already get some insight in the data.
def plotData(data):
	plt.figure()
	plt.plot(data['date'], data['downloads'])
	plt.xlabel('date')
	plt.ylabel('amount')
	plt.title('Amount of downloads')
	plt.show()

#plotData(data)

#b. Can you find the season size?
def determineSeasonSize(data):
	plt.figure()
	plt.acorr(data['downloads'].astype(float))
	plt.xlabel('offset')
	plt.ylabel('correlation')
	plt.title('Auto-correlation revenues')
	plt.show()

#better
def determineSeasonSizeStatsmodel(data):
	plt.figure()
	plot_acf(data['downloads'].astype(float), lags=15)
	plt.xlabel('offset')
	plt.ylabel('correlation')
	plt.title('Auto-correlation revenues')
	plt.show()

#determineSeasonSize(data)
#determineSeasonSizeStatsmodel(data)

#c. Forecast now, for each prediction method, the next three days. Calculate the MAE, RMSE and
#MAPE each time. Complete the following table with all results:

def calculateSeason(period):
	SandR = pd.Series(data['downloads']) - trend
	n = len(SandR)
	seasonal = []
	for i in range(0, period):
		seasonal = seasonal + [SandR[list(range(i, n, period))].mean()]
	print(seasonal)

#Our solution:

# def calculatePreviousForecasting(past, predictor):
# 	predicted = []
# 	n = len(past)
# 	for i in range(0,n):
# 		predicted = predicted + [predictor(past[0:i])]
# 	return predicted


# def naiveForecasting(past):
# 	if (len(past)<1):
# 		return math.nan
# 	return past[len(past)-1]

# def averageForecasting(past):
# 	if (len(past)<1):
# 		return math.nan
# 	return pd.Series(past).mean()

# def movingAverageForecasting(period):
# 	def result(past):
# 		n = len(past)
# 		if (n < period):
# 			return math.nan
# 		return pd.Series(past[(n-period):n]).mean()
# 	return result

# def calculateWeights(period, past):
#     n = len(past)
#     if (n<2*period):
#         return math.nan
#     v = past[(n-2*period):(n-period)]
#     for i in range(2,period+1):
#         v = v + past[(n-2*period+i-1):(n-period+i-1)]
#     M = np.array(v).reshape(period, period)
#     v = past[(n-period):n]
#     return np.linalg.solve(M, v)

# def linearCombinationForecasting(period):
# 	#for linear combination: data has to be a list (not numpy array, df column,..) -> ex. print(linearCombinationForecasting(13)(list(data['downloads']))) works
#     def result(past):
#         n = len(past)
#         if (n<2*period):
#             return math.nan
#         a = calculateWeights(period, past)
#         return (past[(n-period):n]*a).sum()
#     return result

# def general_regression(x, y, degree=1, exp=False):
#     func=lambda x:x # def fun(x): return[x]
#     inv_func=lambda x:x
#     if (exp):
#         func=np.exp
#         inv_func=np.log
#     model = np.polyfit(x, inv_func(y), degree)
#     line = np.poly1d(model)
#     predict = lambda x:func(line(x))
#     yyy = pd.Series(predict(x))
#     se = math.sqrt(((yyy-y)**2).mean())
#     R2 = (x.corr(inv_func(y)))**2
#     result = [se, R2, predict]
#     index = ['se', 'R2', 'predict']
#     for i in range(1,len(model)+1):
#         result = np.append(result, model[-i])
#         index += chr(i+96) # to obtain the characters a,b,...
#     result = pd.Series(result)
#     result.index = index
#     return result

# def trendForecastingModel(past):
# 	n = len(past)
# 	x = pd.Series(range(0,n))
# 	y = pd.Series(past)
# 	reg = general_regression(x, y)
# 	return reg.predict

# def calculateMAE(data):
# 	return data.abs().mean()

# def calculateRMSE(data):
# 	return math.sqrt((data**2).mean())

# def calculateMAPE(data, past):
# 	return (data/past).abs().mean()

def calculateMultipleDays(data, amountOfDays):
 	for i in range(0, amountOfDays):
 		#change function to whatever is needed
 		processedData = linearCombinationForecasting(13)(list(data))
 		print(processedData)
 		data = data.append(pd.Series(result.observed))

# #specific function for trend forecasting
# def calculateMultipleDaysTrendForecasting(data, amountOfDays):
# 	for i in range(0, amountOfDays):
# 		processedData = trendForecastingModel(data)
# 		dataLength = len(data)
# 		print(processedData(dataLength))
# 		data = data.append(pd.Series(processedData(dataLength)))

# def calculateSeason(period):
# 	SandR = pd.Series(data['downloads']) - trend
# 	n = len(SandR)
# 	seasonal = []
# 	for i in range(0, period):
# 		seasonal = seasonal + [SandR[list(range(i, n, period))].mean()]
# 	print(seasonal )

# def findTrend(x, period):
# 	result = smooth(x, period)
# 	nan = [math.nan] * int(period/2)
# 	if (period % 2 == 0):
# 		result = smooth(result, 2)
# 	result = nan + result + nan
# 	return result

# def smooth(x, period):
# 	result = []
# 	for i in range(0, len(x)-period+1):
# 		result = result + [np.mean(x[i:i+period])]
# 	return result

# #Heads up: change to list(data['downloads']) when using linearCombinationForecasting, when using multiple

# #predicted = calculatePreviousForecasting(data['downloads'], naiveForecasting)
# #predicted = calculatePreviousForecasting(data['downloads'], averageForecasting)

# #can replace the second parameter in calculatePreviousForecasting with forecast if needed (both work the same)
# #Working for movingAverageForecasting and linear
# #forecast = movingAverageForecasting(5)
# #predicted = calculatePreviousForecasting(data['downloads'], forecast)
# #errors = pd.Series(predicted) - data['downloads']

# #calculating errors with trend for MAE, RMSE and MAPE:
# myTrend = trendForecastingModel(data['downloads'])

#31 = the amount of data (can use len() because amount varies from dataframe to dataframe, 31 is the amount in this scenario)
# #predicted = pd.Series(myTrend(range(0,31)))

# predicted = pd.Series(myTrend(range(0,len(data))))

# errors = predicted - data['downloads']

#other method
#forecast = movingAverageForecasting(5)
#forecast(data['downloads'])

#print(movingAverageForecasting(5)(data['downloads']))
#print(linearCombinationForecasting(13)(list(data['downloads'])))

#calculateMultipleDaysTrendForecasting(data['downloads'], 3)

#print(calculateMultipleDays(data['downloads'], 3))

#Calculate MAE, RMSE and MAPE

#print(calculateMAE(errors))
#print(calculateRMSE(errors))
#print(calculateMAPE(errors, data['downloads']))

#Prediction method, day 31, day 32, day 33, MAE, 		RMSE, 		MAPE
#Naïve: 			34, 	34, 	34, 	1.533333, 	1.914854, 	0.1199493
#Average:			17.4387,17.48387,14.48387,8.2082828,9.302767,	0.4338687
#Moving (m=5)		30.6,	31.120,	31.344,	3.130769,	3.366578,	0.1835417
#Linear comb(m=13)	36.8321,37.3841,42.0988,1.531631,	1.780848,	0.0488565
#Trend 				33.30968,34.29879,35.28790,1.0036420,1.156301,	0.104034
#additive decomp.	
#multip. decomp.	

result = smts.seasonal_decompose(data['downloads'], model='additive', period=7)
print(result.observed)
# print(calculateMultipleDays(downloads, 3))

#Dominik's solution:
def calculate_previous_forecasting(data, predictor):
    predicted = []
    n = len(data)
    for i in range(0, n):
        predicted = predicted + [predictor(data[0:i])]
    return predicted


def calculate_mae(past, predictor):
    predicted = calculate_previous_forecasting(past, predictor)
    errors = pd.Series(predicted) - past
    return errors.abs().mean()


def calculate_rmse(past, predictor):
    predicted = calculate_previous_forecasting(past, predictor)
    errors = pd.Series(predicted) - past
    return math.sqrt((errors ** 2).mean())


def calculate_mape(past, predictor):
    predicted = calculate_previous_forecasting(past, predictor)
    errors = pd.Series(predicted) - past
    return (errors / past).abs().mean()


def naive_forecasting(data):
    if len(data) < 1:
        return math.nan
    return data.iloc[len(data) - 1]


naive_predictions = [naive_forecasting(data['downloads']) for i in range(3)]
naive_errors = {'mae': calculate_mae(data['downloads'], naive_forecasting), 'rmse': calculate_rmse(data['downloads'], naive_forecasting),
                'mape': calculate_mape(data['downloads'], naive_forecasting)}


def average_forecasting(data):
    if len(data) < 1:
        return math.nan
    return pd.Series(data).mean()


average_predictions = data['downloads'].copy()
average_predictions = average_predictions.append(pd.Series([average_forecasting(average_predictions)]),
                                                 ignore_index=True)
average_predictions = average_predictions.append(pd.Series([average_forecasting(average_predictions)]),
                                                 ignore_index=True)
average_predictions = average_predictions.append(pd.Series([average_forecasting(average_predictions)]),
                                                 ignore_index=True)

average_errors = {'mae': calculate_mae(data['downloads'], average_forecasting), 'rmse': calculate_rmse(data['downloads'], average_forecasting),
                  'mape': calculate_mape(data['downloads'], average_forecasting)}


def moving_avg_forecasting(period):
    def result(data):
        n = len(data)
        if (n < period):
            return math.nan
        return pd.Series(data[(n - period):n]).mean()

    return result


moving_avg_5 = moving_avg_forecasting(5)

moving_avg_5_predictions = data['downloads'].copy()
moving_avg_5_predictions = moving_avg_5_predictions.append(pd.Series(moving_avg_5(moving_avg_5_predictions)),
                                                           ignore_index=True)
moving_avg_5_predictions = moving_avg_5_predictions.append(pd.Series(moving_avg_5(moving_avg_5_predictions)),
                                                           ignore_index=True)
moving_avg_5_predictions = moving_avg_5_predictions.append(pd.Series(moving_avg_5(moving_avg_5_predictions)),
                                                           ignore_index=True)


def calculateWeights(period, past):
    past = [*past]
    n = len(past)
    if (n < 2 * period):
        return math.nan
    v = past[(n - 2 * period):(n - period)]
    for i in range(2, period + 1):
        v = v + past[(n - 2 * period + i - 1):(n - period + i - 1)]
    M = np.array(v).reshape(period, period)
    v = past[(n - period):n]
    return np.linalg.solve(M, v)


def linearCombinationForecasting(period):
    def result(past):
        n = len(past)
        if (n < 2 * period):
            return math.nan
        a = calculateWeights(period, past)
        return (past[(n - period):n] * a).sum()

    return result


linear_comb_13 = linearCombinationForecasting(13)

linear_comb_predictions = data['downloads'].copy()

linear_comb_predictions = linear_comb_predictions.append(pd.Series(linear_comb_13(linear_comb_predictions)),
                                                         ignore_index=True)
linear_comb_predictions = linear_comb_predictions.append(pd.Series(linear_comb_13(linear_comb_predictions)),
                                                         ignore_index=True)
linear_comb_predictions = linear_comb_predictions.append(pd.Series(linear_comb_13(linear_comb_predictions)),
                                                         ignore_index=True)


def general_regression(x, y, degree=1, exp=False, log=False):
    data = pd.DataFrame({'x': x, 'y': y})
    data.reset_index(drop=True, inplace=True)
    func = lambda x: x  # def func(x): return[x]
    inv_func = lambda x: x
    if (exp):
        func = np.exp
        inv_func = np.log
    # elif (log):
    # func = lambda x: math.log(x)
    # inv_func = lambda x: x.apply(lambda z: math.log(z))
    sy = data.y.std()
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x: func(line(x))
    data['y_pred'] = pd.Series(predict(x))
    se = math.sqrt(((data.y_pred - data.y) ** 2).mean())
    R2 = 1 - (se ** 2) / (sy ** 2)
    result = [se, R2, predict]
    index = ['se', 'R²', 'predict']
    for i in range(1, len(model) + 1):
        result = np.append(result, model[-i])
        index += chr(i + 96)  # to obtain the characters a,b,...

    result = pd.Series(result)
    result.index = index
    return result


def trend_forecasting_model(data):
    n = len(data)
    x = pd.Series(range(0, n))
    y = pd.Series(data)
    reg = general_regression(x, y)
    return reg



trend_estimation = trend_forecasting_model(data['downloads'])

trend_estimation_predictions = pd.Series(
    [trend_estimation.predict(n), trend_estimation.predict(n + 1), trend_estimation.predict(n + 2)])


def smooth(x, period):
    result = []
    for i in range(0, len(x) - period + 1):
        result = result + [np.mean(x[i:i + period])]
    return result


def find_trend(x, period):
    result = smooth(x, period)
    nan = [math.nan] * int(period / 2)
    if period % 2 == 0:
        result = smooth(result, 2)
    result = nan + result + nan
    return result


period = 7
trend = find_trend(data['downloads'], period)

S_and_R = pd.Series(data['downloads']) - find_trend(data['downloads'], period)
season_factor = []
for i in range(0, period):
    season_values = S_and_R[list(range(i, len(S_and_R), period))]
    season_values = season_values[~(np.isnan(season_values))]
    season_factor = season_factor + [season_values.mean()]

plt.figure()
plt.plot(range(0, len(season_factor)), season_factor, 'o-')
plt.show()

seasonal = []
for i in range(0, len(data['downloads'])):
    seasonal = seasonal + [season_factor[i % period]]

# TODO: shift range by dropped na
shift = 0
for i in range(n):
    if trend[i] is math.nan:
        shift += 1
    else:
        break

general_trend = general_regression([*range(0+shift, len(pd.Series(trend).dropna())+shift)], pd.Series(trend).dropna())
print('Formula for seasonal trend line: y = {} + {}*x'.format(general_trend.a,general_trend.b))

pd.Series(trend).dropna()
plt.figure()
plt.plot(range(0, len(trend)), trend, 'o-')
plt.plot(range(0, len(trend)), [general_trend.predict(n) for n in range(0, len(trend))], '-', color='orange')
plt.show()

[general_trend.predict(n) for n in range(0, len(trend))]
general_trend.predict(10)


#d. Linear combination: Which weights do you find? Which value plays the greatest role in predicting
#the next value?

#print(calculateWeights(13,list(data['downloads'])))

#e. Trend estimation: What is the formula of the regression line?
myTrend = trend_forecasting_model(data['downloads'])
#print(myTrend(0))

#f. Additive decomposition: What is the formula for the trend line?


#g. Additive decomposition: What are the values for the recurring pattern (seasonal trend)?
trend = find_trend(data['downloads'], 7)
calculateSeason(7)

#h. Multiplier decomposition: What is the formula for the trend line?

#i. Multiplicative decomposition: What are the values for the recurring pattern (seasonal trend)?

#j. Which technique gives the best prediction and why? --> linear, in this situation this has the lowest MAPE value (the lower, the more accurate)