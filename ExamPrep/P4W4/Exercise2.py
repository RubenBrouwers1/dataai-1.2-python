import scipy.spatial.distance as dist
import pandas as pd
from sklearn.neighbors import DistanceMetric
import numpy as np
from scipy import stats
import matplotlib
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier

#a. Load the file "students.csv". This file contains the scores of 100 students in certain subjects
students = pd.read_csv('students.csv')
print(students)

#b. Calculate the distance between student1 and student2 in the following ways:
#Euclidean
#Standardized euclidian
#Manhattan

#Note: with
#dist(example,example, metric='cityblock')
#you can calculate the Manhattan distance between all the points.
def standardizedEuclidianDistance(data, val1, val2):
	zScores = data.copy() / data.std(axis=0)
	print('Standardized euclidian distance is', dist.euclidean(zScores.iloc[val1], zScores.iloc[val2]))

print('Euclidean distance is', dist.euclidean(students.iloc[0], students.iloc[1]))
standardizedEuclidianDistance(students, 0, 1)
print('Manhattan distance is', dist.cityblock(students.iloc[0], students.iloc[1]))

#c. We are looking for four clusters in this set. Choose the following centroids:
#(9, 3, 14, 1, 6, 10, 10, 15)
#(12, 18, 9, 5, 18, 1, 3, 18)
#(6, 15, 13, 18, 9, 15, 20, 18)
#(5, 4, 7, 18, 20, 17, 1, 15)
#Now calculate the Euclidean distance of students 1, 10, 20 and 30 to these centroids.

def lookForClusters():
	centroids = pd.DataFrame({0:[9, 3, 14, 1, 6, 10, 10, 15],1:[12, 18, 9, 5, 18, 1, 3, 18],2:[6, 15, 13, 18, 9, 15, 20, 18],3:[5, 4, 7, 18, 20, 17, 1, 15]})
	centroids = centroids.transpose()
	for student_index in [1,10,20,30]:
	    # index_centroid = 0
	    for centroid in centroids.index:
	        # print(centroid, 'centroid', index_centroid)
	        # print(centroid,centroids.iloc[centroid])
	        # print(centroid)
	        distance = dist.euclidean(students.iloc[student_index-1], centroids.iloc[centroid])
	        print('Euclidean distance of student {} to centroid {} is {}'.format(student_index, centroid + 1, distance))

#lookForClusters()

#d. Which centroid would you assign these students to? --> look at answer for c
#student 1: first centroid
#student 2: first centroid
#student 3: third centroid
#student 4: third centroid

#e. Determine the categories (clusters) of each student using the K-means algorithm (4 clusters). How many students are in each category?
kmeansmodel = KMeans(n_clusters=4)
kmeansmodel.fit(students)

students_kmeans = students.copy()
students_kmeans['cluster'] = kmeansmodel.labels_

def kMeansAlgorithm():
	print(students_kmeans['cluster'].value_counts())

#25 per cluster
#kMeansAlgorithm()

#kMeansAlgorithm(students, 4)
#f. Add a column containing the category. Use DecisionTreeClassifier in Python to create a decision tree.
def showID3Tree():
	classifier = DecisionTreeClassifier(criterion='entropy')
	classifier.fit(students_kmeans.drop(labels='cluster', axis=1), students_kmeans['cluster'])
	tree.plot_tree(classifier, feature_names=students_kmeans.drop(labels='cluster',axis=1).columns.values, class_names=['{} cluster'.format(i) for i in np.unique(students_kmeans['cluster'])], filled=True, fontsize=6)
	plt.show()

#showID3Tree()

#g. How would you characterize these four categories?
#To find out: look at ID3 tree from ex. f
#First cluster: students who can't manage subject2 nor subject1,
#Third cluster:students who can manage subject 2 and subject4 reasonably well, but
#have no top score on subject3,
#The other two clusters are hard to describe


#h. In which category does a new student fall with the following points : (10, 15, 12, 11, 13, 14, 9, 10)?
def calculateNewStudent():
	newStudent = pd.DataFrame([10,15,12,11,13,14,9,10])
	newStudent = newStudent.transpose()

	#this number can be different every time
	print(kmeansmodel.predict(newStudent))

calculateNewStudent()