# Principal Component Analysis (PCA)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

studenq_raw = pd.read_csv("csv/studenq.csv", delimiter=';',decimal='.')

studenq_raw.info() # 1° and last columns (‘ID’ and ‘class’) have to be removed.
studenq = studenq_raw.drop(['ID','class'], axis=1)

studenq = studenq.dropna(axis='rows')

studenq.corr()

studenqZ = StandardScaler().fit_transform(studenq)
#Better:
studenqZ =pd.DataFrame()
for column in studenq:
    mean = studenq[column].mean()
    standev = studenq[column].std()
    studenqZ[column] = (studenq[column] - mean)/standev
studenqZ.index= studenq.index.values

pca_dim = min(studenqZ.shape[1], studenqZ.shape[0])
pcamodel = PCA(pca_dim)
principalComponents = pcamodel.fit_transform(studenqZ)

print('Explained variance in %:', pcamodel.explained_variance_ratio_)

labels_bar = ['PC{}'.format(i) for i in range(1,pca_dim+1)]

plt.figure()
plt.bar(labels_bar,pcamodel.explained_variance_ratio_)
plt.title('PCA: Variance ratio')
plt.xlabel('PCi')
plt.ylabel('%')
plt.show()

principalDf= pd.DataFrame()
principalDf['PC1'] = principalComponents[:,0]
principalDf['PC2'] = principalComponents[:,1]
principalDf['cat'] = studenq_raw['class']
fig, ax = plt.subplots(figsize=(5, 5))
ax.set_xlabel('Principal Component 1')
ax.set_ylabel('Principal Component 2')
ax.set_title('2-dim PCA')
labels_plot = set(principalDf['cat'])
markers = list(matplotlib.markers.MarkerStyle.markers.keys())
for m, l in zip(markers, labels_plot):
    indices = np.where(principalDf['cat'] == l)[0]
    ax.scatter(principalDf.loc[indices, 'PC1'], principalDf.loc[indices, 'PC2'], marker=m, s=50)
ax.legend(labels_plot)
plt.show()

row_labels = ['PC{}'.format(i) for i in range(1,pca_dim+1)]
aij=pd.DataFrame(data=pcamodel.components_,

columns = studenq.columns, index = row_labels)

print(aij)

col_names = ['PC{}'.format(i) for i in range(1,7)]
new_studenq_data = pd.DataFrame(data=principalComponents[:,[0,1,2,3,4,5]], columns= col_names)