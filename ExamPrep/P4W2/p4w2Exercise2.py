#Exercise 2
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
import math
import numpy as np
import statsmodels.tsa.seasonal as smts

#always look at the delimiter, in this case it's ';'. If it's something else than a comma then you will get an error when trying to get columns
data = pd.read_csv("amusement park.csv", delimiter=';')

#a. Make a graph of the data. What do you notice visually? Is the number of visitors increasing or
#decreasing? Is there a recognizable pattern? --> There is an ever increasing seasonal size in visitors.

#b. Would you use the additive or multiplier model?
#A multiplier. The seasonal trend increases.

def plotData(data):
	plt.figure()
	plt.plot(data['year'], data['month'], data['number of visitors'])
	plt.xlabel('period')
	plt.ylabel('visitors')
	plt.title('Amount of visitors')
	plt.show()

#plotData(data)

#c. What is the size of 1 "season"?
#The size of one season is 12 months. You can see this on the graph, there is a clear peak on the offsets of 12.
def determineSeasonSizeStatsmodel(data):
	plt.figure()
	plot_acf(data['number of visitors'].astype(float), lags=30)
	plt.xlabel('offset')
	plt.ylabel('correlation')
	plt.title('Auto-correlation revenues')
	plt.show()

#determineSeasonSizeStatsmodel(data)

#d. Now do a decomposition. Plot this in a diagram.
def seasonalDecomposition(data):
	result=smts.seasonal_decompose(data, model='multiplier', period=12)
	plt.figure()
	result.plot()
	plt.show()

#seasonalDecomposition(data['number of visitors'])

def general_regression(x, y, degree=1, exp=False):
    func=lambda x:x # def fun(x): return[x]
    inv_func=lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x:func(line(x))
    yyy = pd.Series(predict(x))
    se = math.sqrt(((yyy-y)**2).mean())
    R2 = (x.corr(inv_func(y)))**2
    result = [se, R2, predict]
    index = ['se', 'R2', 'predict']
    for i in range(1,len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

def trendForecastingModel(past):
	n = len(past)
	x = pd.Series(range(0,n))
	y = pd.Series(past)
	reg = general_regression(x, y)
	return reg.predict

def calculateMAE(data):
	return data.abs().mean()

def calculateRMSE(data):
	return math.sqrt((data**2).mean())

def calculateMAPE(data, past):
	return (data/past).abs().mean()

def calculateWeights(period, past):
    n = len(past)
    if (n<2*period):
        return math.nan
    v = past[(n-2*period):(n-period)]
    for i in range(2,period+1):
        v = v + past[(n-2*period+i-1):(n-period+i-1)]
    M = np.array(v).reshape(period, period)
    v = past[(n-period):n]
    return np.linalg.solve(M, v)

def linearCombinationForecasting(period):
	def result(past):
		n = len(past)
		if (n<2*period):
			return math.nan
		a = calculateWeights(period,past)
		return (past[(n-period):n]*a).sum()
	return result

myTrend = trendForecastingModel(data['number of visitors'])
predicted = pd.Series(myTrend(range(0,len(data))))
errors = predicted - data['number of visitors']

forecast = linearCombinationForecasting(12)
print(forecast(list(data['number of visitors'])))

print(calculateMAE(errors))
print(calculateRMSE(errors))
print(calculateMAPE(errors, data['number of visitors']))