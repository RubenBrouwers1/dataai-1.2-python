import pandas as pd
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

#Question 1 a:
w5qBroadBand = pd.read_csv('broadband.csv', delimiter=',', decimal=',')
#to print multiple columns: print(w5qBroadBand[['area','avr sync speed']])
w5qBroadBand['avr sync speed'] = w5qBroadBand['avr sync speed'].dropna()
#Question 1 b:
w5qBroadBand['rounded sync speed'] = round(w5qBroadBand['avr sync speed'],0)

#convert to int
w5qBroadBand['rounded sync speed'] = w5qBroadBand['rounded sync speed'].astype(int)

#Question 1 c: What is the mode, median and mean of the rounded sync_speed?

"""print(w5qBroadBand['rounded sync speed'].mode())
print(w5qBroadBand['rounded sync speed'].median())
print(w5qBroadBand['rounded sync speed'].mean())
print('\nRemoved outliers')"""

#Question 1 d: Remove outliers
q_low = w5qBroadBand['rounded sync speed'].quantile(0.01)
q_hi  = w5qBroadBand['rounded sync speed'].quantile(0.99)
w5qBroadBand['rounded sync speed'] = w5qBroadBand['rounded sync speed'][(w5qBroadBand['rounded sync speed'] < q_hi) & (w5qBroadBand['rounded sync speed'] > q_low)]

"""print(w5qBroadBand['rounded sync speed'].mode())
print(w5qBroadBand['rounded sync speed'].median())
print(w5qBroadBand['rounded sync speed'].mean())"""

#grab specific column by it's index

#Question 2:
"""print(w5qBroadBand.iloc[:, 2].sum() / w5qBroadBand.iloc[:, 2].count())
print(w5qBroadBand.iloc[:, 2].median())
print(w5qBroadBand.iloc[:, 2].mode())"""

#Question 5:Put the following scores with their corresponding frequencies in a numpy vector

"""q5NpVectorScore = [20,21,25,26]
q5NpVectorF = [4,2,3,3]
print(q5NpVectorScore)
print(q5NpVectorF)"""

#Write a function that can calculate the average from this named vector.
def calculateWeightedAverage(score, weight):
	"""Example np arrays
	weight = [4,2,3,3]
	score = [20,21,25,26]"""

	return np.average(score, weights=weight)

#Now calculate the average score with this function (4 significant figures).
score = [20,21,25,26]
weight = [4,2,3,3]
print(calculateWeightedAverage(score, weight))

#We have the following scores: 3, 6, 9, 22, 9, 5, 8, 21, 9, 5, 3, 8, 9. To which central tendency is the score 9 equal?
scoresArray = np.array([3, 6, 9, 22, 9, 5, 8, 21, 9, 5, 3, 8, 9])
print(scoresArray.mean())
print(np.median(scoresArray))
print(stats.mode(scoresArray))
#--> mean and mode :)

#Write functions that can calculate the mode and median of a named vector
"""def central(numbers):
	meanNumbers = numbers.mean()
	#for dataframe, for np array: np.median(numbers)
	medianNumbers = numbers.median()
	#for dataframe, for np array: stats.mode(numbers)
	modeNumbers = numbers.mode()
	geometric = stats.gmean(numbers)
	harmonic = 0
	stringCentral = "Mean: " + str(meanNumbers) + "\nMedian :" + str(medianNumbers) + "\nMode: " + str(modeNumbers) + "\nGeometric: " + str(geometric)
	return print(stringCentral)"""

def central(data):
	mode = data.mode()
    #for dataframe, for np array:
	#mode = stats.mode(data)
	median = data.median()
    #for dataframe, for np array:
	#median = np.median(data)
	mean = data.mean()
	#np.mean(data)
	geometric = np.exp(np.mean(np.log(data)))
	harmonic = 1/np.mean(1/data)
	return print("Mode: " + str(mode) + "\nMedian :" + str(median) + "\nMean: " + str(mean) + "\nGeometric: " + str(geometric) + "\nHarmonic: " + str(harmonic))

#central(scoresArray)
central(w5qBroadBand['rounded sync speed'])
"""
q8TableProvincesChildren = {'# of children per family' : [1,2,3,4,5,6,7],
							'Antwerp' : [60,55,30,20,10,5,10],
							'Limburg' : [65,60,35,20,10,5,5]}

q8TableProvincesChildren = pd.DataFrame(q8TableProvincesChildren, columns=['# of children per family','Antwerp','Limburg'])

#Question 8 a and b answer checks
print(central(q8TableProvincesChildren['Antwerp']))
print('De limburg')
print(central(q8TableProvincesChildren['Limburg']))
print('eikes')
print(calculateWeightedAverage(q8TableProvincesChildren['# of children per family'],q8TableProvincesChildren['Limburg']))"""

#Question 10:
ageMeans = [37.7,31.4]
weightingPercent = [73,27]
print(calculateWeightedAverage(ageMeans,weightingPercent))

#Question 11:
hoursPerDay = [3,2]
amountOfPeopleByGender = [10,30]
print(calculateWeightedAverage(hoursPerDay,amountOfPeopleByGender))

#Question 12:
boardMemberAmount = [300,10,500]
boardMemberAgeAverage = [25,60,45]
print(calculateWeightedAverage(boardMemberAgeAverage,boardMemberAmount))

print(w5qBroadBand['rounded sync speed'].describe())