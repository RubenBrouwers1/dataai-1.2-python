import pandas as pd
import matplotlib.pyplot as plt

studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter = ';', decimal=";")

# How many % of students have 3 or fewer siblings?
amount = (studenq['Siblings'] < 3).sum()
percentage = ((amount / studenq['Siblings'].sum()) * 100).sum()
print(percentage)

# How many students with blood type O have
# smoked in the past year?
# amount = (studenq['Smoker'] == "Yes" )
print(pd.crosstab(studenq['Blood Type'], studenq.Smoker, margins=True))

# Create a pie chart of the number of pieces of fruit
# the students eat per day.
x = studenq.Fruit.value_counts().sort_index()
l = ['1 or less', '2', '4']
plt.figure()
plt.pie(x, labels=l)
plt.title('Pieces of fruit per week')
plt.show()

# How divided are the students' views on the number
# of years Biden will live in the White House? (barplot
# or hist?)
x = studenq.Biden.value_counts().sort_index()
l = ['1 Year', '2 Years', '3 Years', '4 Years', '5 Years', '8 Years']
plt.figure()
plt.bar(l, x)
plt.title('Biden in Whitehouse')
plt.xlabel('Years')
plt.ylabel('Number')
plt.show()

# 5. Visualize the students' favourite leisure activities
# depending on their place of residence (Antwerp or
# not).
ct = pd.crosstab(studenq['Antwerp Address'], studenq['Leisure'], margins=False)
plt.figure()
ct.plot.bar(title='Leisure in Antwerp')
plt.xlabel('Antwerp Address')
plt.show()

