# Seasonal forecasting
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.tsa.seasonal as smts
from statsmodels.graphics.tsaplots import plot_acf
revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]

# data = trend + season + noise
# xi = T(i) + S(i) + R(i)

# S(i) + R(i) = xi − T(i) (additive)
# S(i) ⋅ R(i) = xi/T(i) (multiplicative)

# Determining the season size
# This plot is a graph with all auto-correlation values.
plt.figure()
plt.acorr(np.array(revenues).astype(float))
plt.xlabel('offset')
plt.ylabel('correlation')
plt.title('Auto-correlation revenues')
plt.show()

# or

plt.figure()
plot_acf(np.array(revenues).astype(float),lags=15)
plt.xlabel('offset')
plt.ylabel('correlation')
plt.title('Auto-correlation revenues')
plt.show()

# Determining the trend 
# will calculate the moving average of a series of numbers.
def smooth(x, period):
    result = []
    for i in range(0, len(x)-period+1):
        result = result + [np.mean(x[i:i+period])]
    return result

def findTrend(x, period):
    result = smooth(x, period)
    nan = [math.nan] * int(period/2)
    if (period % 2 == 0):
        result = smooth(result, 2)
    result = nan + result + nan
    return result

# Determining T(t)
# Assume: seasonal size = m

# Take moving average with length m of
# data (take average along 2 sides)

# Result: T(t) = general trend, without
# seasonal trend and without noise

# If you want to forecast the general
# trend, you are going to approximate
# these values with a (linear)
# regressionresultaat: T(t) = a + b * t

period = 4
trend = findTrend(revenues, period)
print(trend)

# Determining S(t)
# Subtract T(t) from the data (or divide by T(t)) You now only
# have seasonal component and noise

# For each season, take only the values of the season

# Remove the na-values

# For each season, take the average of the values. This yields
# the pattern of S(t)

SandR = pd.Series(revenues) - trend
seasonFactor = []
for i in range(0, period):
    seasonValues = SandR[list(range(i, len(SandR), period))]
    seasonValues = seasonValues[~(np.isnan(seasonValues))]
    seasonFactor = seasonFactor + [seasonValues.mean()]

# Determining R(t)
# Subtract T(t) and S(t) from
# the original data (or divide).

seasonal = []
for i in range(0, len(revenues)):
    seasonal = seasonal+[seasonFactor[i%period]]
noise = pd.Series(revenues) - trend - seasonal
mae = noise.abs().mean()
rsme = math.sqrt((noise **2).mean())
mape = (noise/revenues).abs().mean()

# Forecasting 
# You can now predict values for
# a given time tx. Suppose the
# data follows the additive model
# 
# f(t) = T(t) + S(t)
# 
# Since we cannot predict the
# noise R(t), we leave it out.

low = period//2
high = len(revenues)-period//2

# determine the best fitting regression

best_fit_Rsquare (pd.Series(range(low,high)),
pd.Series(result.trend[low:high]))

# regression - cubic

reg = general_regression(pd.Series(
range(low, high)), pd.Series(trend[low: high]), 3)
# Forecast

tx = 25
predicted = reg.predict(range(tx, tx+1)) + seasonFactor[tx%period]

result=smts.seasonal_decompose(
revenues, model='additive', period=4)

# TODO: Where does this trend come from?
SandR = pd.Series(revenues) - trend
n = len(SandR)
seasonal = []
for i in range(0, 4):
    seasonal = seasonal + [SandR[list(range(i, n, 4))].mean()]
print(seasonal )

