import numpy as np
import pandas as pd

#Question 1: Create a table with 4 columns
#Question 2: Copy the 2nd column from the table into a variable. What data type does this variable have? DataFrame
df=pd.DataFrame({"Id":[1,2,3,4,5], 
                 "Age":[15,23,11,18,20],
                 "Weight":[54,68,41,59,65],
                 "Temp":[36.5,37.1,36.4,37.2,36.9]}) 
#To change column names -> df.columns = ['..','...'..]
#To change index names -> df.index = ['..','..'..]

"""Question 3: show the 3rd row off the table: print(df.iloc[[2]]) -> index is 0 based
also a solution:
thirdRow = df[df["Id"] == 3] -> select row where the Id column = 3 (first created column)
print(thirdRow.head())"""
print("Question 3: show the 3rd row off the table\n")
print(df.iloc[[2]])

#Question 4: calculate the minimum and maximum age: .min() and .max() -> convert to str or else you can't concat properly (values are int)
print("Question 4: calculate the minimum and maximum age\n")
print(str(df["Age"].min()) + ", " +  str(df["Age"].max()))

#Question 5: Calculate the sum of all weights and share that by the number of weights -> .sum() / .count()
print("Question 5: Calculate the sum of all weights and share that by the number of weights\n")
print("Total weight: " + str(df["Weight"].sum()) + ", Amount of rows: " + str(df["Weight"].count()) + ", Average: " + (str(df["Weight"].sum() / df["Weight"].count())))

#Question 6: Divide all weights by 1000 -> use simple function
print("Question 6: Divide all weights by 1000")
def divideWeight1000(x):
	return x / 1000
def multiplyWeight1000(x):
	return x * 1000

df["Weight"] = df["Weight"].apply(divideWeight1000)
print(df)
df["Weight"] = df["Weight"].apply(multiplyWeight1000)

#Question 7: Convert all temperatures to Fahrenheit (formula: celsius * 1.8 + 32)
print("Question 7: Convert all temperatures to Fahrenheit\n")
def convertToFahrenheit(x):
	return x * 1.8 + 32

df["Temp"] = df["Temp"].apply(convertToFahrenheit)
print(df)

#Question 8: Save the table in a file -> .to_csv
df.to_csv('data.csv')

#Question 9: Read in the table in IntelliJ (Python) as a data frame. Change something in it and save again.

#Question 10: Read the table from the file -> .read_csv
print("Question 10: Read the table from the file\n")
readDf = pd.read_csv('data.csv')
print(readDf)

#Question 11: Delete the "id" column -> del df['columnName']
print('Question 11: Delete the "id" column\n')
del df["Id"]
print(df)

#Question 12: Add a column "length" with data: 160, 175, 162, 169, 179
print('Question 12: Add a column "length" with data: 160, 175, 162, 169, 179\n')
df.insert(3, "Length", [160,175,162,169,179])
print(df)

#Question 13: Calculate the BMI of each person in the table
print("Question 13: Calculate the BMI of each person in the table\n")
#Can be put in a function
BMI = 0.0
for index, row in df.iterrows():
	BMI = float((row["Weight"]) / (row["Length"] / 100))
	print(BMI)
print(BMI)

#Question 14: Add this BMI as a column to the table
print("Question 14: Add this BMI as a column to the table\n")
df["BMI"] = ""
#df["BMI"] = np.nan
BMI = 0.0
for index, row in df.iterrows():
	BMI = float((row["Weight"]) / (row["Length"] / 100))
	df.set_value(index, "BMI ", BMI)
print(df)

#Question 15: Retain only the weights that are smaller or equal to 60 from the weight column.
print("Question 15: Retain only the weights that are smaller or equal to 60 from the weight column.\n")
dfCopy = df.copy()
dfCopy.drop(dfCopy[dfCopy["Weight"] < 60].index, inplace=True)
print(dfCopy)

#Question 16: In the age column, replace all ages greater or equal to 18 by 19
print("Question 16: In the age column, replace all ages greater or equal to 18 by 19\n")
dfCopy = df.copy()
dfCopy.loc[dfCopy["Age"] >= 18, "Age"] = 19
print(dfCopy)

#Question 17: Find all temperatures of persons over 16 years of age
print("Question 17: Find all temperatures of persons over 16 years of age")
print(df.loc[df["Age"] > 16, "Temp"])

"""print("amount in specific rows\n")
print(df.loc[df["Age"] == 15, "Weight"].sum())"""